const { request } = require("express");
const config = require('config')
const {Router} = require("express")
const Rate = require('../models/Rate')
const User = require('../models/User')
const router = Router()
const {check,validationResult} = require('express-validator');
const { Mongoose } = require("mongoose");



router.post(
    '/add',
    [
        check('name','Неверное имя').isLength({min:2}),



    ],
    async(req,res)=>{
        try {
            const Errors = validationResult(req)
            console.log(Errors)
            if(!Errors.isEmpty()){
                return res.status(400).json({
                    Errors: Errors.array(),
                    message: 'Некорректные данные!'
                })  
            }
            const {name,priceInternet,priceBuzzer,priceSMS,totalSum,InternetLimit,BuzzerLimit,SmsLimit} = req.body
            console.log("Req.body",req.body)
            const candidate = await Rate.findOne({name})
            console.log("candidate:",candidate)
            if(candidate){
                return res.status(400).json({message:"Такой тариф уже существует!"})
            }
            if(priceInternet=='' && priceBuzzer=='' && priceSMS==''){
                if(InternetLimit==''){
                    return res.status(400).json({message:"Введите количество ГБ в месяц"})
                }
                if(BuzzerLimit==''){
                    return res.status(400).json({message:"Введите количество минут в месяц!"})
                }
                if(totalSum==''){
                    return res.status(400).json({message:"Введите месячную сумму!"})
                }
                if(SmsLimit==''){
                    return res.status(400).json({message:"Введите количество SMS в месяц!"})
                }
            }
            if(priceInternet!='' || priceSMS!='' || priceBuzzer!=''){
                if(priceBuzzer==''){
                    return res.status(400).json({message:"Введите стоимость одной минуты!"})
                }
                if(priceInternet=='')
                    return res.status(400).json({message:"Введите стоимость одного Гб интернета!"})
                if(priceSMS=='')
                    return res.status(400).json({message:"Введите стоимость одного сообщения"})
            }
           
            const rate = new Rate({name,priceInternet,priceBuzzer,priceSMS,totalSum,InternetLimit,BuzzerLimit,SmsLimit})
            /*await Rate.insertMany({"name":req.body.name,"priceInternet":req.body.priceInternet,"priceBuzzer":req.body.priceBuzzer,
                                "priceSMS":req.body.priceSMS,"totalSum":req.body.totalSum,"InternetLimit":req.body.InternetLimit,
                                "BuzzerLimit":req.body.BuzzerLimit,"SmsLimit":req.body.SmsLimit},function(err,rate){
                                    if(err) console.log(err)
                                    else console.log("Rate")
                                })*/
            //console.log("Rate:", rate)
            await rate.save() 
            res.status(201).json({message:"Тариф успешно создан!"})
        } catch (e) {

            res.status(500).json({message:"Что-то пошло ни так, попробуйте снова!"})
        }
})

router.post(
    '/changed',
    [
        check('name','Неверное имя').isLength({min:2}),

    ],
    async(req,res)=>{
        try {
            const Errors = validationResult(req)
            if(!Errors.isEmpty()){
                return res.status(400).json({
                    Errors: Errors.array(),
                    message: 'Неккоректные данные при изменении!'
                })
            }
            const {name,priceInternet,priceBuzzer,priceSMS,totalSum,InternetLimit,BuzzerLimit,SmsLimit} = req.body
            //console.log({name,priceInternet,priceBuzzer,priceSMS,totalSum,InternetLimit,BuzzerLimit,SmsLimit})
            const candidate = await Rate.findOne({name})
    
            if(!candidate){
                return res.status(400).json({message:"Тариф не найден!"})
            }
            let TOTALSum,PRICEBuzzer,PRICEInternet,PRICESMS,INTERNETLimit,BUZZERLimit,SMSLimit
            if(req.body.priceBuzzer!=''){
                PRICEBuzzer = req.body.priceBuzzer
            }
            else{
                PRICEBuzzer = candidate.priceBuzzer
            }

            if(req.body.priceInternet!=''){
                PRICEInternet = req.body.priceInternet
            }
            else{
                PRICEInternet = candidate.priceInternet
            }

            if(req.body.priceSms!=''){
                PRICESMS = req.body.priceSms
            }
            else{
                PRICESMS = candidate.priceSms
            }

            if(req.body.InternetLimit!=''){
                INTERNETLimit = req.body.InternetLimit
            }
            else{
                INTERNETLimit = candidate.InternetLimit
            }

            if(req.body.BuzzerLimit!=''){
                BUZZERLimit = req.body.BuzzerLimit
            }
            else{
                BUZZERLimit = candidate.BuzzerLimit
            }

            if(req.body.SmsLimit!=''){
                SMSLimit = req.body.SmsLimit
            }
            else{
                SMSLimit = candidate.SmsLimit
            }
            if(req.body.totalSum!=''){
                TOTALSum = req.body.totalSum 
            }
            else{
                TOTALSum = candidate.totalSum
            }
            const id = candidate.id
            console.log(typeof(req.body.name))
            console.log(candidate._id)
            /*await Rate.findByIdAndUpdate(candidate._id,{name:req.body.name,priceInternet:req.body.priceInternet,priceBuzzer:req.body.priceBuzzer,
            priceSMS:req.body.priceSMS,totalSum:TOTALSum,InternetLimit:req.body.InternetLimit,BuzzerLimit:req.body.BuzzerLimit,SmsLimit:req.body.SmsLimit},{new: true},
            function(err,rate){
                //Mongoose.disconnect();
                if(err) return console.log(err)
                console.log("Обновленные объект", Rate.findById(candidate._id))
            })*/
            await Rate.findByIdAndUpdate(candidate._id,{name:req.body.name,priceInternet:PRICEInternet,priceBuzzer:PRICEBuzzer,
            priceSMS:PRICESMS,totalSum:TOTALSum,InternetLimit:INTERNETLimit,BuzzerLimit:BUZZERLimit,SmsLimit:SMSLimit},{new: true},
            function(err,rate){
                //Mongoose.disconnect();
                if(err) return console.log(err)
                console.log("Обновленные объект", Rate.findById(candidate._id))
            })
           
            res.status(201).json({message:"Тариф успешно изменен!"})
        } catch (e) {

            res.status(500).json({message:"Что-то пошло ни так, попробуйте снова!"})
        }
})

router.post(
    '/delete',
    [

    ],
    async(req,res)=>{
        try {
            const Errors = validationResult(req)
            if(!Errors.isEmpty()){
                return res.status(400).json({
                    Errors: Errors.array(),
                    message: 'Неккоректные данные при удалении!'
                })
            }
            const {name,priceInternet,priceBuzzer,priceSMS,totalSum,InternetLimit,BuzzerLimit,SmsLimit} = req.body
            //console.log({name,priceInternet,priceBuzzer,priceSMS,totalSum,InternetLimit,BuzzerLimit,SmsLimit})
            console.log(req.body)
            await Rate.deleteOne({name : req.body.ratename})
            res.status(201).json({message:"Тариф успешно удален!"})
        } catch (e) {

            res.status(500).json({message:"Что-то пошло ни так, попробуйте снова!"})
        }
})

router.get('/',async(req,res) =>{
    try {
         const rates = await Rate.find()
         res.json(rates)
    } catch (e) {
        res.status(500).json({message:"Что-то пошло ни так, попробуйте снова!"})
    }
})


module.exports = router
