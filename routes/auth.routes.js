const {Router}= require('express')
const User = require('../models/User')
const bcrypt = require('bcryptjs')
const config = require('config')
const jwt = require('jsonwebtoken')
const auth = require('../middleware/auth.meddaleware')
const {check, validationResult} =require('express-validator')
const router = Router()

// /api/auth/register
router.post(
    '/register',
    [
        check('email','Неверный email').isEmail(),
        check('password','Минимальная длина пароля 6 символов').isLength({min:6}),
        check('userphone','Неверный номер телефона').isMobilePhone(),
        check('username','Неверное имя! Минимальная длина 2 символа').isLength({min:2}),
        check('userlastname','Неверная фамилия!').isLength({min:5})
    ],
    async (req,res)=>{
    try {
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).json({
                errors: errors.array(),
                message:'Неккоректные данные при регистрации!'
            })
        }

        //console.log(req.body)
        const {email,userphone,username,userlastname,password,rate} = req.body
        const candidate = await User.findOne({phone:userphone})
        if(candidate){
            return res.status(400).json({message:'Такой пользователь уже существует!'})
        }

        const hashedPassword = await bcrypt.hash(password, 12)
        const user = new User({email,password:hashedPassword,phone:userphone,username,lastname:userlastname,rate})
        
        await user.save()
        res.status(201).json({message: 'Пользователь создан!'})

    } catch (e) {
        res.status(500).json({message:'Что-то пошло ни так! Попробуйте снова!'})
    }
})

// /api/auth/login
router.post(
    '/login',
    [
        check('password','Минимальная длина пароля 6 символов').exists(),
        check('phone','Неверный номер телефона').isMobilePhone(),
    ],
    async (req,res)=>{
    try {
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).json({
                errors: errors.array(),
                message:'Неккоректные данные при входе!'
            })
        }

        const {phone,password} = req.body
        const user = await User.findOne({phone})
        
        if(!user){
            return res.status(400).json({message: "Пользователь не найден!"})
        }

        const isMAth = await bcrypt.compare(password,user.password)

        if(!isMAth){
            return res.status(400).json({message: "Неверный пароль, попробуйте снова!"})
        }

        token = jwt.sign(
            {   userId: user.id, 
                username:user.username,
                userlastname:user.lastname,
                userphone:user.phone,
                userRate:user.rate
            },
            config.get('jwtSecret'),
            {
                expiresIn:'1h'
            }
        )
        res.json({token,userId:user.id,userName:user.username,userlastname:user.lastname,userphone:user.phone,userRate:user.rate})
    } catch (e) {
        res.status(500).json({message:'Что-то пошло ни так! Попробуйте снова!'})
    }
})

router.post(
    '/binding',
    [

    ],

    async(req,res)=>{
        try {
            // mongoose.set("useFindAndModify", false);
            const errors = validationResult(req)
            if(!errors.isEmpty()){
                return res.status(400).json({
                    errors: errors.array(),
                    message:'Неккоректные данные!'
                })
            }
            User.findByIdAndUpdate(req.body.userId,{rate:req.body.ratename},function(err,user){
                //Mongoose.disconnect();
                if(err) return console.log(err)
                console.log("Обновленные объект", User.findById(req.body.userId))
            })
            //await user.save()
            res.status(201).json({message:"Тарифный план успешно выбран!"})
        } catch (e) {
            res.status(500).json({message:"Что-то пошло ни так,попробуйте снова!!!!!"})
        }
})

router.get('/:id',async(req,res) =>{
    try {
         const user = await User.findById(req.params.id)
         console.log('User',user)
         //console.log('userphone',userphone)
         res.json(user)
    } catch (e) {
        res.status(500).json({message:"Что-то пошло ни так, попробуйте снова!"})
    }
})

module.exports = router
