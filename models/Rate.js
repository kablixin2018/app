const {Schema,model, Types} = require('mongoose')

const schema = new Schema({
    name:{type:String,require:false,unique:false},
    priceInternet:{type:Number,require:false,default:0},
    priceBuzzer:{type:Number,require:false,default:0},
    priceSMS:{type:Number,require:false,default:0},
    totalSum:{type:Number,require:false,default:0},
    InternetLimit:{type:Number,require:false,default:0},
    BuzzerLimit:{type:Number,require:false,default:0},
    SmsLimit:{type:Number,require:false,default:0},
}
)
module.exports= model('Rate',schema)