const{ Schema, model,Types} = require('mongoose')

const schema = new Schema({
    email:{type:String, required:true, unique:false},
    password:{type:String,required:true},
    phone:{type:Number,required:true, unique:true},
    username:{type:String,required:true},
    lastname:{type:String,required:true},
    rate:{type:String,require:false,unique:false,default:null}
})

module.exports = model('User',schema)