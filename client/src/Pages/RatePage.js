import React,{useCallback, useContext, useEffect, useState} from 'react'
import { AuthContext } from '../context/AuthContex'
import { useHttp } from '../hooks/http.hook'
import {Loader} from '../Components/Loader'
import{RateList} from '../Components/RateList'
import '../style/price.css'

export const RatePage = () =>{
    const [rate,setRate] = useState([])
    const {loading,request} =  useHttp()
    const {token,username} = useContext(AuthContext)

    const fetchRate = useCallback(async ()=>{
        try {zz
           const fetch = await request('/api/rate','GET',null,{
               Authorization: `Bearer ${token}`
           })
           setRate(fetch)
        } catch (e) {
            
        }
    },[token,request])
    useEffect(()=>{
        fetchRate()
    },[fetchRate])
    
    if(loading){
        return <Loader/>
    }
    return(
        <>
            {!loading && <RateList  rate={rate} user={username}/>}
        </>
    )
}