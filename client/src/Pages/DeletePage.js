import React,{useEffect, useState,useCallback, useContext} from 'react'
import {useHttp} from '../hooks/http.hook'
import {useMessage} from '../hooks/message.hook'
import {RateAdminList} from '../Components/RateAdminList'
import {Loader} from '../Components/Loader'
import {AuthContext} from '../context/AuthContex'

export const DeletePage = () =>{
    const [rate,setRate] = useState([])
    const {loading,request} =  useHttp()
    const {token,username} = useContext(AuthContext)

    const fetchRate = useCallback(async ()=>{
        try {
           const fetch = await request('/api/rate','GET',null,{
               Authorization: `Bearer ${token}`
           })
           setRate(fetch)
        } catch (e) {
            
        }
    },[token,request])
    useEffect(()=>{
        fetchRate()
    },[fetchRate])
    
    if(loading){
        return <Loader/>
    }
    return(
        <>
            {!loading && <RateAdminList  rate={rate} user={username}/>}
        </>
    )
}