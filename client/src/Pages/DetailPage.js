import React, { useCallback, useEffect, useState,useContext } from 'react'
import {useParams} from 'react-router-dom'
import { Loader } from '../Components/Loader'
import { UserCard } from '../Components/UserCard'
import { AuthContext } from  '../context/AuthContex'
import { useHttp } from '../hooks/http.hook'

export const DetailPage = () =>{
    const {request,loading} = useHttp()
    const[user,setUser] = useState(null)
    const {userId,token} = useContext(AuthContext)
    //const userId = useParams().id
    console.log('Id: ',userId)
    const getParamUser = useCallback(async ()=>{
        try {
            const userRet = await request('/api/auth/' + userId,'GET')
            //console.log('UserRate',userRet)
            setUser(userRet)
        } catch (e) {}
    },[request,userId])

    useEffect(()=>{
        getParamUser()
    },[getParamUser])

    if(loading){
        return <Loader/>
    }
    return(
        <>
        {!loading && user && <UserCard user={user}/>}
        </>
    )
}