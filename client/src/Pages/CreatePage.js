import React,{useEffect, useState} from 'react'
import {useHttp} from '../hooks/http.hook'
import {useMessage} from '../hooks/message.hook'
import '../style/CreatePage.css'

export const CreatePage = () =>{
     //const[rate,setRate] = useState('')
     const message =  useMessage()
     const {loading,error,request,clearError}=useHttp()

     useEffect( () =>{
         message(error)
         clearError()
     },[error, message,clearError])

     const [form, setForm] = useState({
         name: '', priceInternet: '', priceBuzzer:'',  priceSMS: '', totalSum: '',InternetLimit:'',BuzzerLimit:'',SmsLimit:'',
     })

     const changeHandler = event =>{
         setForm({...form, [event.target.name]:event.target.value })
     }

     const createHandler = async () =>{
         try {
             const data = await request('/api/rate/add', 'POST',{...form})
             message(data.message)
         } catch (e) {}
     }

     return(
        <div className="container main">
            <div className="row">
                <div className="col-md ">
                    <form className="">
                        <div className="form-group">
                            <label htmlFor="name" className="cols-sm-2 control-label">Название</label>
                            <input className="form-control"  id="name" type="text" name="name" placeholder="Название"
                            onChange={changeHandler} />
                            
                        </div>
                        <div className="form-group">
                            <label htmlFor="priceInternet" className="cols-sm-2 control-label">Цена за 1 гб интерента(если необходимо)</label>
                            <input  className="form-control" placeholder="Цена за интернет" id="priceInternet" type="text" name="priceInternet"
                             onChange={changeHandler} />
                            
                        </div>
                        <div className="form-group">
                            <label htmlFor="priceBuzzer" className="cols-sm-2 control-label">Цена за 1 минуту вызова(если необходимо)</label>
                            <input  className="form-control" placeholder="Цена за звонок" id="priceBuzzer" type="text" name="priceBuzzer" 
                            onChange={changeHandler}/>
                           
                        </div>
                        <div className="form-group">
                            <label htmlFor="priceSMS" className="cols-sm-2 control-label">Цена за 1 сообщение(если необходимо)</label>
                            <input  className="form-control" placeholder="Цена за интернет" id="priceSMS" type="text" name="priceSMS"
                            onChange={changeHandler}  />
                            
                        </div>
                        <div className="form-group">
                            <label htmlFor="totalSum" className="cols-sm-2 control-label">Цена тарифа в месяц</label>
                            <input  className="form-control" placeholder="Цена за тариф в месяц" id="totalSum" type="text" name="totalSum"
                            onChange={changeHandler}  />
                        </div>
                        <div className="form-group">
                            <label htmlFor="InternetLimit" className="cols-sm-2 control-label">Объем трафика интернета</label>
                            <input  className="form-control" placeholder="Объем трафика интернета" id="InternetLimit" type="text" name="InternetLimit" 
                            onChange={changeHandler}  /> 
                        </div>
                        <div className="form-group">
                            <label htmlFor="BuzzerLimit" className="cols-sm-2 control-label">Количество минут в месяц</label>
                            <input  className="form-control" placeholder="Количество минут в месяц" id="BuzzerLimit" type="text" name="BuzzerLimit"  
                            onChange={changeHandler}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="SmsLimit" className="cols-sm-2 control-label">Количество сообщений в месяц</label>
                            <input  className="form-control" placeholder="Количество сообщений в месяц" id="SmsLimit" type="text" name="SmsLimit" 
                            onChange={changeHandler}  />
                        </div>
                        <div className="card-action">
                            <button
                            className="btn_enter btn yellow darken-4"
                            onClick={createHandler}
                            disabled={loading}
                            >
                                Создать
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
     )
}