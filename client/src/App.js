import './App.css'
import React from 'react'
import {BrowserRouter as Router} from 'react-router-dom'
import { useRoutes } from './routes';
import {useAuth} from './hooks/auth.hook'
import {AuthContext} from './context/AuthContex'
import {Header} from './Components/Header'
import {HeaderAdmin} from './Components/HeaderAdmin'

function App() {
  const {login,logout,token,userId,userName,userlastname,userphone,userrate}=useAuth()
  const isAuthenticated = !!token
  const routes = useRoutes(isAuthenticated)
  if(userId =="608185cd1e0870071085a6bb"){
    return (
      <AuthContext.Provider value={{
       login,logout,token,userId,userName,userlastname,userphone,userrate,isAuthenticated 
      }}>
         <Router>
           {isAuthenticated&&<HeaderAdmin/>}
           <div className="containers">
             {routes}
           </div>
       </Router>
      </AuthContext.Provider>
     );
  }
  else{
    return (
      <AuthContext.Provider value={{
       login,logout,token,userId,userName,userlastname,userphone,userrate,isAuthenticated 
      }}>
         <Router>
           {isAuthenticated&&<Header/>}
           <div className="containers">
             {routes}
           </div>
       </Router>
      </AuthContext.Provider>
     );
  }
  
}

export default App
