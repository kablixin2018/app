import React from 'react'
import {Switch, Route, Redirect} from 'react-router-dom'
import {AboutPage} from './Pages/AboutPage'
import {CreatePage} from './Pages/CreatePage'
import {DetailPage} from './Pages/DetailPage'
import {AuthPage} from './Pages/AuthPage'
import {RatePage} from './Pages/RatePage'
import {ChangedPage} from './Pages/ChangedPage'
import {DeletePage} from './Pages/DeletePage'
export const useRoutes = isAuthenticated => {
    if(isAuthenticated){
        return(
            <Switch>
                <Route path="/about/:id" >
                    <AboutPage/>
                </Route>
                <Route path="/add" exact>
                    <CreatePage/>
                </Route>
                <Route path="/detail" exact>
                    <DetailPage/>
                </Route>
                <Route path="/rate" >
                    <RatePage/>
                </Route>
                <Route path="/changed" >
                    <ChangedPage/>
                </Route>
                <Route path="/delete" >
                    <DeletePage/>
                </Route>
                <Redirect to="rate" />
            </Switch>
        )
    }
    return(
        <Switch>
            <Route path="/" exact>
                <AuthPage/>
            </Route>
            <Redirect to="/" />
        </Switch>

    )
}