import React, { useContext } from 'react'
import {NavLink, useHistory} from 'react-router-dom'
import {AuthContext} from '../context/AuthContex'


export const HeaderAdmin = () =>{
    const History = useHistory()
    const auth = useContext(AuthContext)


    const logoutHandler = event =>{
        event.preventDefault()
        auth.logout()
        History.push('/')
    }
    
    
    return(
        <div className="container">
            <div className="row">
                <div className="col-md main_link">
                    <span><a href="/">Личный кабинет</a></span>
                </div>
                <div className="col-md hamburger_menu">
                    <input id="menu_toggle" type="checkbox" />
                    <label className="menu_btn" for="menu_toggle">
                        <span></span>
                    </label>
                    <ul className="menu_box">
                        <li><NavLink className="menu_item" to="/detail">Управление аккаунтом</NavLink></li>
                        <li><NavLink className="menu_item" to="/add">Создать тариф</NavLink></li>
                        <li><NavLink className="menu_item" to="/changed">Изменить тариф</NavLink></li>
                        <li><NavLink className="menu_item" to="/delete">Удалить тариф</NavLink></li>
                        <li><a className="menu_item" href="/" onClick={logoutHandler}>Выйти</a></li>
                    </ul>
                </div>
            </div>
            <div className="row">
                <div className="col-md-6 text-header">
                    <hr />
                    <span>Вы управляете: {auth.userphone }</span>
                </div>
            </div>
        </div>
    )
}