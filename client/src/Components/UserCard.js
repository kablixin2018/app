import React, { useContext } from 'react'
import { AuthContext } from '../context/AuthContex'
import '../index.css'
export const UserCard= ({user}) =>{
    const data = useContext(AuthContext)
    return(
        <>
           <div className="container about_user">
                    <p>Ваше имя: {data.userName}</p>
                    <p>Ваше фамилия {data.userlastname}</p>
                    <p>Ваш телефон {data.userphone}</p>
                    <p>Ваш тариф: {user.rate}</p>
           </div>
        </>
    )
}