import React,{ useEffect, useState, useContext } from 'react'
import '../style/price.css'
import {useHttp, } from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'
import {AuthContext} from '../context/AuthContex'



export const RateList = ({rate},{username}) =>{
    const auth = useContext(AuthContext)
    
    const message = useMessage()
    
    const {loading,error,request,clearError}=useHttp()
    
    const [form]=useState({
      userId:auth.userId,ratename:""
    })
   
    useEffect(()=>{
        message(error)
        clearError()  
      },[error,message,clearError])

    const addrate = (ratename) =>{
        form.ratename = ratename
        console.log({...form})
        changedHandler()
    }
    const changedHandler = async () =>{
        try {
          const data = await request('/api/auth/binding','POST',{...form})
          message(data.message)
        } catch (e) {}
      }



    if(!rate.length){
        return <p className="center">Тарифов пока нет!</p>
    }
    let smsNotLimit,InternetNotLimit,BuzzerNotLimit,priceNotLimit,InternerPrice
    return(
        <div className="container pricing-container">
            <div className="row">
                {rate.map(rate=>{
                        if(rate.SmsLimit>0){
                            smsNotLimit=false
                        }
                        else{
                            smsNotLimit=true
                        }
                        if(rate.InternetLimit>0 && rate.InternetLimit!=null){
                            InternetNotLimit=false
                        }
                        else{
                        InternetNotLimit=true
                        }
                        if(rate.priceInternet>0 && rate.priceInternet!=null){
                            InternerPrice=true
                        }
                        else{
                            InternerPrice = false
                        }
                        if(rate.BuzzerLimit>0){
                            BuzzerNotLimit=false
                        }
                        else{
                            BuzzerNotLimit=true
                        }
                        if(rate.totalSum>0){
                            priceNotLimit=true
                        }
                        else{
                            priceNotLimit=false
                        }
                        return(
                            <div className="col-md-3" >
                                <div className="plan">
                                    <div className="paln-head planone">
                                        <span>{rate.name}</span>
                                    </div>
                                <div className="plans-body">
                                    <ul>
                                    <li>
                                        <i className="fa fa-database" aria-hidden="true"></i>
                                        <span>{InternetNotLimit ? InternerPrice ?<span>{rate.priceInternet} рублей 1 Гб</span>:<span>Безлимитный итнернет</span>:<span>{rate.InternetLimit} Гб в месяц </span>}</span>
                                        
                                    </li>
                                    <li>
                                        <i className="fa fa-download" aria-hidden="true"></i>
                                        <span>{BuzzerNotLimit ?<span>{rate.priceBuzzer} рублей в минуту</span>: <span>{rate.BuzzerLimit} минут в месяц </span> }</span>
                                    </li>
                                    <li>
                                        <i className="fa fa-microchip" aria-hidden="true"></i>
                                        <span>{BuzzerNotLimit ?<span>{rate.priceSMS} рубля одно сообщение</span>: <span>{rate.SmsLimit} сообщений в месяц </span> }</span>
                                    </li>
                                    </ul>
                                </div>
                                <div className="plans-footer">
                                    <p>{priceNotLimit ? <p>{rate.totalSum} р/месяц</p>:'все зависит от вас'}</p>
                                    <button
                                    type="button" 
                                    value={rate.name}        
                                    className="btn btn-default" 
                                    //onChange={rateHandler}
                                    onClick={()=>addrate(rate.name)}
                                    disabled={loading}
                                    >
                                        Установить
                                    </button>
                                </div>
                                </div>
                            </div>

                        )
                    })}
            </div>
        </div>
    )
}