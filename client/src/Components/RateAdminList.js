import React,{ useEffect, useState, useContext } from 'react'
import '../style/price.css'
import {useHttp} from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'
import {AuthContext} from '../context/AuthContex'

export const RateAdminList = ({rate},{username}) =>{
    const auth = useContext(AuthContext)
    
    const message = useMessage()
    
    const {loading,error,request,clearError}=useHttp()
    
    const [form]=useState({
      userId:auth.userId,ratename:""
    })
   
    useEffect(()=>{
        message(error)
        clearError()  
      },[error,message,clearError])

    const deleterate = (ratename) =>{
        form.ratename = ratename
        console.log({...form})
        changedHandler()
    }
    const changedHandler = async () =>{
        try {
          const data = await request('/api/rate/delete','POST',{...form})
          message(data.message)
        } catch (e) {}
      }



    if(!rate.length){
        return <p className="center">Тарифов пока нет!</p>
    }
    let smsNotLimit,InternetNotLimit,BuzzerNotLimit,priceNotLimit,InternerPrice
    return(
        <div className="container pricing-container">
            <div className="row">
                {rate.map(rate=>{
                        if(rate.SmsLimit>0){
                            smsNotLimit=false
                        }
                        else{
                            smsNotLimit=true
                        }
                        if(rate.InternetLimit>0){
                            InternetNotLimit=false
                        }
                        else{
                        InternetNotLimit=true
                        }
                        if(rate.priceInternet>0){
                            InternerPrice=true
                        }
                        if(rate.BuzzerLimit>0){
                            BuzzerNotLimit=false
                        }
                        else{
                            BuzzerNotLimit=true
                        }
                        if(rate.totalSum>0){
                            priceNotLimit=true
                        }
                        else{
                            priceNotLimit=false
                        }
                        return(
                            <div className="col-md">
                                <div className="plan">
                                    <div className="paln-head planone">
                                        <span>{rate.name}</span>
                                    </div>
                                <div className="plans-body">
                                    <ul>
                                    <li>
                                        <i className="fa fa-database" aria-hidden="true"></i>
                                        <span>{InternetNotLimit ? InternerPrice ? <span>{rate.priceInternet} рублей 1 Гб</span>  :'Безлимитный интеренет':<span>{rate.InternetLimit} Гб в месяц </span>}</span>
                                    </li>
                                    <li>
                                        <i className="fa fa-download" aria-hidden="true"></i>
                                        <span>{BuzzerNotLimit ?<span>{rate.priceBuzzer} рублей в минуту</span>: <span>{rate.BuzzerLimit} минут в месяц </span> }</span>
                                    </li>
                                    <li>
                                        <i className="fa fa-microchip" aria-hidden="true"></i>
                                        <span>{BuzzerNotLimit ?<span>{rate.priceSMS} рубля одно сообщение</span>: <span>{rate.SmsLimit} сообщений в месяц </span> }</span>
                                    </li>
                                    </ul>
                                </div>
                                <div className="plans-footer">
                                    <p>{priceNotLimit ? <p>{rate.totalSum} р/месяц</p>:'все зависит отвас'}</p>
                                    <button
                                    type="button" 
                                    value={rate.name}        
                                    className="btn btn-default" 
                                    //onChange={rateHandler}
                                    onClick={()=>deleterate(rate.name)}
                                    disabled={loading}
                                    >
                                       Удалить
                                    </button>
                                </div>
                                </div>
                            </div>

                        )
                    })}
            </div>
        </div>
    )
}