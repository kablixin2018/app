import {useState,useCallback,useEffect} from 'react'

const storageName = 'userData'

export const useAuth = () => {
    const [token,setToken] = useState(null)
    const [userId,setUserId] = useState(null)
    const [userName,setUserName] = useState(null)
    const [userlastname,setUserlastname] = useState(null)
    const [userphone,setUserphone] = useState(null)
    const [userrate,setUserrate] = useState(null)

    const login = useCallback((jwtToken,userIdInput,userNameInput,userlastNameInput,userphoneInput,userRateInput)=> {
        setToken(jwtToken)
        setUserId(userIdInput)
        setUserName(userNameInput)
        setUserlastname(userlastNameInput)
        setUserphone(userphoneInput)
        setUserrate(userRateInput)

        localStorage.setItem(storageName,JSON.stringify({
            token:jwtToken,userId:userIdInput,userName:userNameInput,userlastname:userlastNameInput,
            userphone:userphoneInput,userrate:userRateInput
        }))
    },[])

    const logout = useCallback(()=> {
        setToken(null)
        setUserId(null)
        setUserName(null)
        setUserlastname(null)
        setUserphone(null)
        setUserrate(null)

        localStorage.removeItem(storageName)
    },[])

    useEffect(()=>{
        const data = JSON.parse(localStorage.getItem(storageName))
        if(data && data.token){
            login(data.token,data.userId,data.userName,data.userlastname,data.userphone,data.userrate)
        }
    },[login])

    return {login,logout,token,userId,userName,userlastname,userphone,userrate}
}
