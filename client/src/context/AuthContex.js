import {createContext} from 'react'

function noop(){}


export const AuthContext = createContext({
    token:null,
    userId: null,
    userName:null,
    userLastName:null,
    userPhone:null,
    userRate:null,
    login: noop,
    logout:noop,
    isAuthenticated: false
})